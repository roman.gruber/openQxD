
/*******************************************************************************
*
* File version.h
*
* Copyright (C) 2009 Martin Luescher
*               2017 Agostino Patella
*
* This software is distributed under the terms of the GNU General Public
* License (GPL)
*
*******************************************************************************/

#ifndef VERSION_H
#define VERSION_H

#define openQCD_RELEASE "openQ*D-1.0"

#endif
